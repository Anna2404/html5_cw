$(document).ready(function () {
	var worker = new Worker("task.js");

	worker.onmessage = function (e) {
		alert(e.data);
	}
	$("#start_counting").click(function () {
		var count_from = $("#count_from").val();
		worker.postMessage({
			'cmd': 'start',
			'count_from': count_from
		});
	});
});
